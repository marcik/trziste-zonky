// webpack.config.js
// This config is webpack-compatible
// and overrides the defaults provided by re-app-builder

module.exports = function () {
    return {
        entry: { // webpack compatible entry section
            app: [
                './src'
            ]
        },
        excludedModules: [ // you can exclude modules from vendor bundle to avoid conflicts
            'koa',
            'koa-ejs'
        ],
        devServer: {
            proxy: {
                '/loans/**': {
                    changeOrigin: true,
                    target: 'https://api.zonky.cz/',
                    onError: () => console.log,
                },
            },
        }
    }
};