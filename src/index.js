import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import axios from 'axios';
import sortBy from 'lodash/sortBy';

import Header from './components/Header';
import LoansList from './components/LoansList';

import './styles/index.scss';
import './components/Filters/index.scss';

class App extends Component {

	constructor(props) {
		super();

		this.state = {
			loans: []
		};

		this.sortTerm = this.sortTerm.bind(this);
		this.sortRating = this.sortRating.bind(this);
		this.sortAmount = this.sortAmount.bind(this);
		this.sortDeadline = this.sortDeadline.bind(this);
		this.loadData = this.loadData.bind(this);
	}

	loadData() {
		axios.get('/loans/marketplace')
			.then(response => {
				this.setState({ loans: sortBy(response.data, ['datePublished']) });
			})
			.catch(error => {
				console.log('Error fetching and parsing data', error);
			});
	}

	componentDidMount() {
		this.loadData();
		setInterval((this.loadData), 300000);
	}

	sortTerm() {
		this.setState({ loans: sortBy(this.state.loans, ['termInMonths']) });
	}

	sortRating() {
		this.setState({ loans: sortBy(this.state.loans, ['rating']) });
	}

	sortAmount() {
		this.setState({ loans: sortBy(this.state.loans, ['amount']) });
	}

	sortDeadline() {
		this.setState({ loans: sortBy(this.state.loans, ['deadline']) });
	}

	render() {
		return (
			<div className="container">
				<Header />
				<div className="Filters">
					Seřadit podle:
					<span className="Filters-btn" onClick={ this.sortTerm }>Doby splácení</span>
					<span className="Filters-btn" onClick={ this.sortRating }>Ratingu</span>
					<span className="Filters-btn" onClick={ this.sortAmount }>Požadované částky</span>
					<span className="Filters-btn" onClick={ this.sortDeadline }>Deadlinu</span>
				</div>
				<LoansList loans={ this.state.loans } />
			</div>
		);
	}
}

ReactDOM.render(
	<App />,
	document.getElementById('root')
);