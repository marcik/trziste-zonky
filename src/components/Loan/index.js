import React, { Component } from 'react';
import Modal from 'react-modal';

import LoanStory from '../LoanStory';
import Detail from '../Detail';

import './index.scss';
import '../Headline/index.scss';
import '../Modal/index.scss';

import logo from '../../img/close.png';

class Loan extends Component {
	constructor(props) {
		super();

		this.state = {
			modalIsOpen: false
		};

		this.openModal = this.openModal.bind(this);
		this.closeModal = this.closeModal.bind(this);
	}

	openModal() {
		this.setState(
			{modalIsOpen: true}
		);
	}

	closeModal() {
		this.setState(
			{modalIsOpen: false}
		);
	}

	render() {
		return(
			<div className="col-md-6">
				<div className="Loan">
					<img src={ this.props.loan.photos[0].url } alt={ this.props.loan.name } className="Loan-img img-responsive" />
					<h2 className="Headline Headline--loan" onClick={ this.openModal }>
						{ this.props.loan.name }
					</h2>
					<LoanStory loan={ this.props.loan } />
				</div>
				<Modal
					isOpen={this.state.modalIsOpen}
					onRequestClose={this.closeModal}
					className="Modal"
					contentLabel="Modal"
				>
					<img src={ logo } className="Modal-close" alt="Close" onClick={this.closeModal} />
					<Detail loan={ this.props.loan } />
				</Modal>
			</div>
		)
	}
}

export default Loan;
