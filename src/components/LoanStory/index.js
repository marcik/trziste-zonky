import React from 'react';

import WordLimit from 'react-word-limit';

const LoanStory = ({ loan }) => {
	if(loan.story !== null) {
		return(
			<p className="Loan-story">
				<WordLimit limit={200}>
					{ loan.story }
				</WordLimit>
			</p>
		)
	} else {
		return(
			<p className="Loan-story">
				{ loan.story }
			</p>
		)
	}
}

export default LoanStory;