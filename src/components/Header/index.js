import React from 'react';

import './index.scss';

import logo from '../../img/logo.png';

const Header = () => {
	return(
		<div className="Header">
			<div className="container">
				<img src={ logo } className="Header-logo" alt="Zonky" />
			</div>
		</div>
	);
}

export default Header;