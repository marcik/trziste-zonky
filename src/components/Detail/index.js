import React from 'react';

import './index.scss';

const Detail = ({ loan }) => {
	return(
		<div className="Detail">
			<div className="row">
				<div className="col-sm-4">
					<img src={ loan.photos[0].url } alt={ loan.name } className="Detail-img img-responsive" />
				</div>
				<div className="col-sm-8">
					<h2 className="Headline Headline--detail">
						{ loan.name }
					</h2>
					<strong>
						{ loan.nickName }
					</strong>
					<div className="Detail-story">
						{ loan.story }
					</div>
				</div>
			</div>
			<div className="row">
				<div className="col-sm-6">
					Rating: <strong>{ loan.rating }</strong>
				</div>
				<div className="col-sm-6">
					Částka: <strong>{ loan.amount } Kč</strong>
				</div>
			</div>
			<div className="row">
				<div className="col-sm-6">
					Počet investic: <strong>{ loan.investmentsCount }</strong>
				</div>
				<div className="col-sm-6">
					Zbývá investovat: <strong>{ loan.remainingInvestment } Kč</strong>
				</div>
			</div>
			<div className="row">
				<div className="col-sm-6">
					Úrok: <strong>{ loan.interestRate * 100  } %</strong>
				</div>
				<div className="col-sm-6">
					Délka splácení: <strong>{ loan.termInMonths } měsíců</strong>
				</div>
			</div>
		</div>
	)
}

export default Detail;