import React from 'react';

import Loan from '../Loan';

import './index.scss';
import '../Headline/index.scss';

const LoansList = (props) => {

	const loans = props.loans;

	const loansItems = loans.map((loan) => {
		return(
			<Loan key={ loan.id } loan={ loan } />
		)
	});

	return(
		<div className="LoansList">
			<h1 className="Headline Headline--main">Tržiště Zonky</h1>
			<div className="row">
				{ loansItems }
			</div>
		</div>
	);
}

export default LoansList;